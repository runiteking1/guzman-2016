from __future__ import division

import numpy as np
from scipy import sparse
from scipy.sparse import linalg


class LinearFiniteElement(object):
    def __init__(self, coordinates, element_node, element_dof, dirchlet_dof, f=None, a=None, bc=None):
        self.coords = coordinates
        self.ELNODE = element_node
        self.ELDOF = element_dof
        self.DIR_DOF = dirchlet_dof

        # RHS
        if f is None:
            self.f = lambda x, y: 1
        else:
            self.f = f

        # Function of LHS on stiffness matrix
        if a is None:
            self.A = lambda x: 1
        else:
            self.A = a

        # Defaults to 0 Dirchlet boundary condition
        if bc is None:
            self.BC = lambda x, y: 0
        else:
            self.BC = bc

    def element_stiffness(self, t1, t2, t3, area, k):
        # A() only effects face in the most uniform way
        a = self.A(k) * np.array([[np.dot(t1, t1), np.dot(t1, t2), np.dot(t1, t3)],
                                  [np.dot(t2, t1), np.dot(t2, t2), np.dot(t2, t3)],
                                  [np.dot(t3, t1), np.dot(t3, t2), np.dot(t3, t3)]])
        return (1 / (4 * area)) * a

    def element_load(self, area, f, k):
        p0 = self.coords[int(self.ELNODE[k, 0])]
        p1 = self.coords[int(self.ELNODE[k, 1])]
        p2 = self.coords[int(self.ELNODE[k, 2])]

        c0 = 1 / 20 * f(p0[0], p0[1]) + 2 / 15 * 1 / 2 * (f((p0[0] + p1[0]) / 2, (p0[1] + p1[1]) / 2) +
                                                          f((p0[0] + p2[0]) / 2, (p0[1] + p2[1]) / 2)) + \
             9 / 20 * 1 / 3 * f((p0[0] + p1[0] + p2[0]) / 3, (p0[1] + p1[1] + p2[1]) / 3)
        c1 = 1 / 20 * f(p1[0], p1[1]) + 2 / 15 * 1 / 2 * (f((p0[0] + p1[0]) / 2, (p0[1] + p1[1]) / 2) +
                                                          f((p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2)) + \
             9 / 20 * 1 / 3 * f((p0[0] + p1[0] + p2[0]) / 3, (p0[1] + p1[1] + p2[1]) / 3)
        c2 = 1 / 20 * f(p2[0], p2[1]) + 2 / 15 * 1 / 2 * (f((p0[0] + p2[0]) / 2, (p0[1] + p2[1]) / 2) +
                                                          f((p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2)) + \
             9 / 20 * 1 / 3 * f((p0[0] + p1[0] + p2[0]) / 3, (p0[1] + p1[1] + p2[1]) / 3)

        a = np.array([[c0], [c1], [c2]])
        return a * area

    def assemble_matrix(self):
        # Initialize some variables
        elements = len(self.ELNODE)  # Number of elements

        stiff_raw = sparse.lil_matrix((int(self.ELDOF.max()) + 1, int(self.ELDOF.max()) + 1))
        force_raw = np.zeros((int(self.ELDOF.max()) + 1, 1))

        # Assemble
        for k in range(elements):
            t0 = -(self.coords[int(self.ELNODE[k, 0])] - self.coords[int(self.ELNODE[k, 1])])
            t1 = -(self.coords[int(self.ELNODE[k, 1])] - self.coords[int(self.ELNODE[k, 2])])
            t2 = -(self.coords[int(self.ELNODE[k, 2])] - self.coords[int(self.ELNODE[k, 0])])

            area = self.herons_area(t0, t1, t2)
            fk = self.element_load(area, self.f, k)
            stiffness_element = self.element_stiffness(t1, t2, t0, area, k)  # EI is just one for now
            for i in range(3):
                iglobal = int(self.ELDOF[k, i])
                force_raw[iglobal] += fk[i]
                for j in range(3):
                    jglobal = int(self.ELDOF[k, j])
                    stiff_raw[iglobal, jglobal] += stiffness_element[i, j]

        # Take care of Dirichet boundary condition
        for i in self.DIR_DOF:
            # First subtract known values from RHS
            force_raw -= self.BC(self.coords[i, 0], self.coords[i, 1]) * stiff_raw[:, i]

            # Zero out the stuff
            stiff_raw[:, i] = 0
            stiff_raw[i, :] = 0
            stiff_raw[i, i] = 1
            force_raw[i] = self.BC(self.coords[i, 0], self.coords[i, 1])

        return sparse.csr_matrix(stiff_raw), force_raw

    @staticmethod
    def herons_area(t0, t1, t2):  # Need to calculate area of a triangle
        t0 = np.linalg.norm(t0)
        t1 = np.linalg.norm(t1)
        t2 = np.linalg.norm(t2)
        s = (t0 + t1 + t2) / 2
        return np.sqrt(s * (s - t0) * (s - t1) * (s - t2))

if __name__ == "__main__":
    # Coordinates
    coords = np.array(
        [[1 / 4, 1 / 4], [0, 5 / 4], [1, 0], [1, 3 / 4], [1, 3 / 2], [3, 0], [5 / 2, 1 / 2], [3 / 2, 1], [2, 3 / 2],
         [3, 1 / 2]])
    ELNODE = np.array(
        [[1, 3, 4], [1, 4, 2], [4, 5, 2], [3, 8, 4], [4, 8, 5], [8, 9, 5], [6, 7, 3], [3, 7, 8], [8, 7, 9], [7, 10, 9],
         [6, 10, 7]])
    DIR_DOF = np.array([[1], [2], [3], [5], [6], [9], [10]])

    # Need to scale to starting at 0
    ELNODE -= 1
    DIR_DOF[:, 0] -= 1
    ELDOF = ELNODE

    # Create finite element object
    fem = LinearFiniteElement(coords, ELNODE, ELDOF, DIR_DOF)
    S, force = fem.assemble_matrix()

    alphas, _ = linalg.cg(S, force)
    # print("--- %s seconds ---" % (time.time() - start_time))
    print(np.dot(force.T, alphas))
