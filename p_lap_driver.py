from __future__ import division

import meshpy.triangle as triangle
import numpy as np
from FiniteElement import FiniteElement
from scipy.sparse import linalg

import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm

def rhs(x,y,p=1.9):
    if x == 0 and y == 0:
        return 0

    # return (2-p)*(x*x + y*y)**(p/2 - 2) * (y*x*x + x*y*y)
    return 2*x*y*(2-p)*(x*x + y*y)**((p-4)/2)

def main(VERBOSE = True):

    p = 1.8

    supnorm = list()
    gradnorm = list()
    mesh_info = triangle.MeshInfo()
    for subd in range(2,7):
        # Create mesh
        nx, ny = (2**subd, 2**subd)
        x = np.linspace(-.5, .5, nx)
        y = np.linspace(-.5, .5, ny)
        xv, yv = np.meshgrid(x, y)
        points = zip(xv.flatten(), yv.flatten())

        mesh_info.set_points(points)
        mesh_info.set_facets(
            [[points.index((-.5, -.5)), points.index((.5, -.5))], [points.index((.5, -.5)), points.index((.5, .5))],
             [points.index((.5, .5)), points.index((-.5, .5))], [points.index((-.5, .5)), points.index((-.5, -.5))]])

        mesh = triangle.build(mesh_info)

        coords = np.array(mesh.points)
        ELNODE = np.array(mesh.elements)
        mesh_attr = np.array(mesh.point_markers)
        DIR_DOF = [i for i in range(len(coords)) if mesh_attr[i]==1]
        ELDOF = ELNODE

        # Create initial guess at p = 2
        fem = FiniteElement(coords, ELNODE, ELDOF, DIR_DOF, BC=lambda x, y: x*y, f=lambda x,y: rhs(x,y,p))
        S, f = fem.assemble_matrix()
        u = linalg.spsolve(S, f)
        # u, info = linalg.cg(S, f, x0 = coords[:,0]*coords[:,1])
        # if info != 0:
        #     print "CG not converged"

        if p < 1.7:
            plist = [1.8, 1.6, 1.5, 1.4, 1.3, 1.22, 1.15, 1.1]
        else:
            plist = [p]

        for incremental_p in plist:
            tol = 1
            it = 0

            print "Running p: ", incremental_p
            print

            while tol > 1e-7 and it < 15:
                old = u
                pgrads = calc_grad(u, incremental_p, coords, ELNODE)

                fem = FiniteElement(coords, ELNODE, ELDOF, DIR_DOF, A=lambda x: pgrads[x], BC=lambda x, y: x*y, f=lambda x,y: rhs(x,y,p))
                S, f = fem.assemble_matrix()
                u = linalg.spsolve(S, f)
                # u, info = linalg.cg(S, f, x0 = coords[:,0]*coords[:,1])
                # if info != 0:
                #     print "CG not converged"

                it += 1
                tol = grad_sq(old - u, coords, ELNODE)

                if VERBOSE:
                    print grad_sq(old - u, coords, ELNODE)
                    print max(np.abs(old - u))
                    print

            if it == 15:
                print "Didn't converge"

        # fig = plt.figure()
        # ax1 = fig.add_subplot(121, projection='3d')
        # ax2 = fig.add_subplot(122, projection='3d')
        # ax1.plot_trisurf(coords[:,0], coords[:,1], u)
        # ax2.plot_trisurf(coords[:,0], coords[:,1], (u - coords[:,0]*coords[:,1]), cmap=cm.jet)
        # plt.show()
        print '-------'
        # print np.dot(f.T, u)
        # print grad_lp(u, coords, ELNODE, p)
        print max(np.abs(u - coords[:,0]*coords[:,1]))
        supnorm.append(max(np.abs(u - coords[:,0]*coords[:,1])))
        print grad_lp(u - coords[:,0]*coords[:,1], coords, ELNODE, p)**(1/p)
        gradnorm.append(grad_lp(u - coords[:,0]*coords[:,1], coords, ELNODE, p)**(1/p))
    # plt.show()
    print supnorm
    print np.log(np.array(supnorm[0:-1])/np.array(supnorm[1:]))/np.log(2)
    print np.log(np.array(gradnorm[0:-1])/np.array(gradnorm[1:]))/np.log(2)


def grad_lp(u, coords, ELNODE, p):
    total = 0
    for el in range(len(ELNODE)):
        # Area
        t0 = -(coords[int(ELNODE[el, 0])] - coords[int(ELNODE[el, 1])])
        t1 = -(coords[int(ELNODE[el, 1])] - coords[int(ELNODE[el, 2])])
        t2 = -(coords[int(ELNODE[el, 2])] - coords[int(ELNODE[el, 0])])

        area = FiniteElement.herons_area(t0, t1, t2)

        p0 = np.append(coords[int(ELNODE[el, 0])], u[int(ELNODE[el, 0])])
        p1 = np.append(coords[int(ELNODE[el, 1])], u[int(ELNODE[el, 1])])
        p2 = np.append(coords[int(ELNODE[el, 2])], u[int(ELNODE[el, 2])])

        normal = np.cross(p1-p0, p2-p0)
        total += area*np.sqrt((normal[0] / normal[2]) ** 2 + (normal[1] / normal[2]) ** 2)**p

    return total


def grad_sq(u, coords, ELNODE):
    total = 0
    for el in range(len(ELNODE)):
        # Area
        t0 = -(coords[int(ELNODE[el, 0])] - coords[int(ELNODE[el, 1])])
        t1 = -(coords[int(ELNODE[el, 1])] - coords[int(ELNODE[el, 2])])
        t2 = -(coords[int(ELNODE[el, 2])] - coords[int(ELNODE[el, 0])])

        area = FiniteElement.herons_area(t0, t1, t2)

        p0 = np.append(coords[int(ELNODE[el, 0])], u[int(ELNODE[el, 0])])
        p1 = np.append(coords[int(ELNODE[el, 1])], u[int(ELNODE[el, 1])])
        p2 = np.append(coords[int(ELNODE[el, 2])], u[int(ELNODE[el, 2])])

        normal = np.cross(p1-p0, p2-p0)
        total += np.sqrt(area*(normal[0] / normal[2]) ** 2 + (normal[1] / normal[2]) ** 2)

    return total

def calc_grad(u, p, coords, ELNODE):
    out = np.zeros((len(ELNODE),))
    for el in range(len(ELNODE)):
        p0 = np.append(coords[int(ELNODE[el, 0])], u[int(ELNODE[el, 0])])
        p1 = np.append(coords[int(ELNODE[el, 1])], u[int(ELNODE[el, 1])])
        p2 = np.append(coords[int(ELNODE[el, 2])], u[int(ELNODE[el, 2])])


        normal = np.cross(p1-p0, p2-p0)
        out[el] = np.sqrt((normal[0]/normal[2])**2 + (normal[1]/normal[2])**2)**(p-2)

        # if el == 10:
        #     print p0, p1, p2, out[el]
        #     print normal

    return out

if __name__ == "__main__":
    main()