from __future__ import division

import meshpy.triangle as triangle
import numpy as np
from FiniteElement import FiniteElement
from scipy.sparse import linalg

def main():
    mesh_info = triangle.MeshInfo()
    mesh_info.set_points([(0,0), (1,0), (1,1), (0,1)], [1,1,1,1])
    mesh_info.set_facets([[0,1],[1,2],[2,3],[3,0]])

    mesh = triangle.build(mesh_info, max_volume=0.005, verbose=False)

    coords = np.array(mesh.points)
    ELNODE = np.array(mesh.elements)
    mesh_attr = np.array(mesh.point_markers)
    DIR_DOF = [i for i in range(len(coords)) if mesh_attr[i]==1]
    ELDOF = ELNODE

    fem = FiniteElement(coords, ELNODE, ELDOF, DIR_DOF, lambda x,y: x*x)
    S, f = fem.assemble_matrix()
    alphas, _ = linalg.cg(S, f)

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import axes3d
    from matplotlib import cm

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_trisurf(coords[:,0], coords[:,1], alphas, cmap=cm.jet)

    plt.show()


if __name__ == "__main__":
    main()


    # alphas, _ = linalg.cg(S, f)
    # print("--- %s seconds ---" % (time.time() - start_time))
    # print np.dot(f.T, alphas)
