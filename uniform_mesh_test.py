from __future__ import division

import meshpy.triangle as triangle
import numpy as np

def refinement_func(tri_points, area):
    max_area=0.1
    return bool(area>max_area)

def main():
    # Build mesh info
    mesh_info = triangle.MeshInfo()
    mesh_info.set_points([(-0.5,-0.5), (0.5,-0.5), (0.5,0.5), (-0.5,0.5)], [1,1,1,1])
    mesh_info.set_facets([[0,1],[1,2],[2,3],[3,0]])

    # Mesh up
    mesh = triangle.build(mesh_info, refinement_func=refinement_func)

    #
    mesh_points = np.array(mesh.points)
    mesh_tris = np.array(mesh.elements)
    mesh_attr = np.array(mesh.point_markers)

    import matplotlib.pyplot as plt
    plt.triplot(mesh_points[:, 0], mesh_points[:, 1], mesh_tris)
    plt.xlabel('x')
    plt.ylabel('y')
    n = np.size(mesh_attr);
    outer_nodes = [i for i in range(n) if mesh_attr[i]==1]
    plt.plot(mesh_points[outer_nodes, 0], mesh_points[outer_nodes, 1], 'go')
    plt.axis([-1.5, 1.5, -1.5, 1.5])
    plt.show()
    #
    # fig = plt.gcf()
    # fig.set_size_inches(4.2, 4.2)
    # plt.savefig('sec5-meshpy-triangle-ex5.pdf')

if __name__ == "__main__":
    main()
